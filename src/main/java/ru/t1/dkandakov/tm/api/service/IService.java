package ru.t1.dkandakov.tm.api.service;

import ru.t1.dkandakov.tm.api.repository.IRepository;
import ru.t1.dkandakov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
